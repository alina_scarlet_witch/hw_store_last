﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace store._2
{
    public class QueueCustomers
    { // Создаем очередь покупателей.
        public Queue<Buyer> Buyers = new Queue<Buyer>();
        // Создаем пять покупателей.
        public Buyer FirstBuyer = new Buyer(1, "Tom", 140);
        public Buyer SecondBuyer = new Buyer(2, "Redl", 265);
        public Buyer ThirdBuyer = new Buyer(3, "Dobby", 300);
        public Buyer FourthBuyer = new Buyer(4, "Ron", 175);
        public Buyer FifthBuyer = new Buyer(5, "Harry", 780);
        public Buyer SixthBuyer = new Buyer(6, "Anna", 7);
        public Buyer SeventhBuyer = new Buyer(7, "Vova", 70);
        public Buyer EightBuyer = new Buyer(8, "Svetlana", 100);
        // Создаем  список покупок.
        // Первый список желаемых товаров.
        ListOfWishes FirstProduct = new ListOfWishes("Milk", 3);
        ListOfWishes SecondProduct = new ListOfWishes("Lemonad", 3);
        ListOfWishes ThirdProduct = new ListOfWishes("Orange", 1);
        // Вторй список желаемых товаров.
        ListOfWishes FourthProduct = new ListOfWishes("Milk", 3);
        ListOfWishes FifthProduct = new ListOfWishes("Chees", 3);
        ListOfWishes SixthProduct = new ListOfWishes("Corn", 6);
        // Третий список желаемых товаров.
        ListOfWishes SeventhProduct = new ListOfWishes("Milk", 3);
        ListOfWishes EightProduct = new ListOfWishes("Cucumbers", 4);
        ListOfWishes NinethProduct = new ListOfWishes("Tomato", 4);
        // Четвртый список желаемых товаров.
        ListOfWishes TenthProduct = new ListOfWishes("Milk", 3);
        ListOfWishes ElevrnthProduct = new ListOfWishes("Coffee", 1);
        ListOfWishes TvelfthProduct = new ListOfWishes("Corn", 5);
        // Пятый список желаемых товаров.  
        ListOfWishes TherteenthProduct = new ListOfWishes("Milk", 1);
        ListOfWishes FourteenthProduct = new ListOfWishes("Chees", 1);
        ListOfWishes FifteenthProduct = new ListOfWishes("Tomato", 6);
        // Шестой список желаемых товаров.  
        ListOfWishes SixteenthProduct = new ListOfWishes("Milk", 2);
        ListOfWishes SeventeenthProduct = new ListOfWishes("Chees", 1);
        ListOfWishes EighteenthProduct = new ListOfWishes("Lemonad", 10);
        // Седьмой список желаемых товаров.  
        ListOfWishes NineteenthProduct = new ListOfWishes("Milk", 3);
        ListOfWishes TwentyProduct = new ListOfWishes("Chees", 1);
        ListOfWishes TwentyOneProduct = new ListOfWishes("Coffee", 1);
        // Восьмой список желаемых товаров.  
        ListOfWishes TwentyTwoProduct = new ListOfWishes("Milk", 3);
        ListOfWishes TwentyThreeProduct = new ListOfWishes("Chees", 1);
        ListOfWishes TwentyFourProduct = new ListOfWishes("Tomato", 6);
        /// <summary>
        /// Метод добавляет товары в список покупателей.
        /// </summary>
        public QueueCustomers()
        {
            //  Помещяем желаемые товары в список первого покупателя.
            FirstBuyer.GoodsList.Add(FirstProduct);
            FirstBuyer.GoodsList.Add(SecondProduct);
            FirstBuyer.GoodsList.Add(ThirdProduct);
            //  Помещяем желаемые товары в список второго покупателя.
            SecondBuyer.GoodsList.Add(FourthProduct);
            SecondBuyer.GoodsList.Add(FifthProduct);
            SecondBuyer.GoodsList.Add(SixthProduct);
            //  Помещяем желаемые товары в список третьего покупателя.
            ThirdBuyer.GoodsList.Add(SeventhProduct);
            ThirdBuyer.GoodsList.Add(EightProduct);
            ThirdBuyer.GoodsList.Add(NinethProduct);
            //  Помещяем желаемые товары в список четвертого покупателя.
            FourthBuyer.GoodsList.Add(TenthProduct);
            FourthBuyer.GoodsList.Add(ElevrnthProduct);
            FourthBuyer.GoodsList.Add(TvelfthProduct);
            //  Помещяем желаемые товары в список пятого покупателя.
            FifthBuyer.GoodsList.Add(TherteenthProduct);
            FifthBuyer.GoodsList.Add(FourteenthProduct);
            FifthBuyer.GoodsList.Add(FifteenthProduct);
            //  Помещяем желаемые товары в список шестого покупателя.
            SixthBuyer.GoodsList.Add(SixteenthProduct);
            SixthBuyer.GoodsList.Add(SeventeenthProduct);
            SixthBuyer.GoodsList.Add(EighteenthProduct);
            //  Помещяем желаемые товары в список седьмого покупателя.
            SeventhBuyer.GoodsList.Add(NineteenthProduct);
            SeventhBuyer.GoodsList.Add(TwentyProduct);
            SeventhBuyer.GoodsList.Add(TwentyOneProduct);
            //  Помещяем желаемые товары в список восьмого покупателя.
            EightBuyer.GoodsList.Add(TwentyTwoProduct);
            EightBuyer.GoodsList.Add(TwentyThreeProduct);
            EightBuyer.GoodsList.Add(TwentyFourProduct); 
            // Добавляем покупателей в очередь.
            Buyers.Enqueue(FirstBuyer);
            Buyers.Enqueue(SecondBuyer);
            Buyers.Enqueue(ThirdBuyer);
            Buyers.Enqueue(FourthBuyer);
            Buyers.Enqueue(FifthBuyer);
            Buyers.Enqueue(SixthBuyer);
            Buyers.Enqueue(SeventhBuyer);
            Buyers.Enqueue(EightBuyer);
        }
       
    }
}
