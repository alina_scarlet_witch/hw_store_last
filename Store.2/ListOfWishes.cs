﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace store._2
{
   public class ListOfWishes
    {
        public string NameProduct;// название продукта
        public int CountProduct;// количество продукта      
        public ListOfWishes(string nameProduct, int countProduct)
        {  // конструктор для записи значений полей      
            NameProduct = nameProduct;
            CountProduct = countProduct;
        }
    }
}
