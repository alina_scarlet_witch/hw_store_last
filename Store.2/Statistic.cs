﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace store._2
{
   public class Statistic
    {       
        public List<Check> OneCheck = new List<Check>();
        public List<Check> AllChecksBuyer = new List<Check>();
        public Storage storage = new Storage();
        public Statistic()
        {
        }
        public void ShowProductsAtTheStorage()
        {
            foreach (var item in storage.CatalogOfProduct)
            {
                Console.WriteLine(item.ProductName + " - " + item.Price + " UAH " + " - " + item.Count);
            }
        }
    }
}
