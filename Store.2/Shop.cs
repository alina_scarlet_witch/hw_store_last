﻿using System;
namespace store._2
{
    public class Shop
    {
        public int IdCheck = 0;
        public static DateTime Time = DateTime.Now;
        // Создаем три полки.
        public Shelf shelfSmall = new Shelf(5);
        public Shelf shelfMedium = new Shelf(10);
        public Shelf shelfBig = new Shelf(20);
        public Shelf shelfAll = new Shelf(20);
        // Создаем экземпляр очереди
        public QueueCustomers queueCostumers = new QueueCustomers();
        public Statistic statistic = new Statistic();
        public Storage storage = new Storage();
        //public Product product = new Product();
        // Создаем товары по умолчанию на полках
        //small
        Product ProductFirst = new Product(1, "Milk", "MilkProducts", 20.0, 3, 2, 5, Time);
        Product ProductSecond = new Product(2, "Yogurt", "MilkProducts", 30.0, 3, 5, 3, Time);
        //Big
        Product ProductFourth = new Product(4, "Lemonad", "DrinksProducts", 24.7, 3, 7, 15, Time);
        Product ProductFifth = new Product(5, "Water", "DrinksProducts", 20.0, 3, 3, 20, Time);
        //Medium
        Product ProductTenth = new Product(10, "Cheryy", "FruitsProducts", 24.7, 3, 7, 11, Time);
        Product ProductEleventh = new Product(11, "Srawberry", "FruitsProducts", 20.0, 3, 10, 10, Time);
        /// <summary>
        /// Конструктор добавляет товары на полки.
        /// </summary>
        public Shop()
        {
            shelfSmall.products.Add(ProductFirst);
            shelfAll.products.Add(ProductFirst);
            shelfSmall.products.Add(ProductSecond);
            shelfAll.products.Add(ProductSecond);
            shelfMedium.products.Add(ProductTenth);
            shelfAll.products.Add(ProductTenth);
            shelfMedium.products.Add(ProductEleventh);
            shelfAll.products.Add(ProductEleventh);
            shelfBig.products.Add(ProductFourth);
            shelfAll.products.Add(ProductFourth);
            shelfBig.products.Add(ProductFifth);
            shelfAll.products.Add(ProductFifth);
        }
        /// <summary>
        /// Метод выводит содержимое полок в консоль.
        /// </summary>
        public void ShowShelfes()
        {
            Console.WriteLine("All Goods in the store:");
            Console.WriteLine("\nGoods on first Shelf:");
            foreach (var item in shelfSmall.products)
            {
                Console.WriteLine(item.ProductName + " - " + item.Price + " UAH " + " - " + item.Count);
            }
            Console.WriteLine("Goods on second Shelf:");
            foreach (var item in shelfMedium.products)
            {
                Console.WriteLine(item.ProductName + " - " + item.Price + " UAH " + " - " + item.Count);
            }
            Console.WriteLine("Goods on third Shelf:");

            foreach (var item in shelfBig.products)
            {
                Console.WriteLine(item.ProductName + " - " + item.Price + " UAH " + " - " + item.Count);
            }
        }
        public void ShowBuyers()
        {
            Console.WriteLine("Queue of customers:");
            foreach (var item in queueCostumers.Buyers)
            {
                Console.WriteLine("\n" + item.ID + " buyer is: " + item.NameBuyer + "\nHis list of coods:");
                foreach (var element in item.GoodsList)
                {
                    Console.WriteLine(element.NameProduct + " - " + element.CountProduct + " pieces.");
                }
            }
        }
        public void Check()
        {
            if (queueCostumers.Buyers.Count == 0)
            {
                Console.WriteLine("\nThere aren't customers in the store.\n");
            }
            else
            {
                do
                {
                    DateTime dateOfCheck = DateTime.Now;
                    ShowShelfes();
                    for (var i = 0; i < shelfAll.products.Count - 1; i++)
                    {
                        for (var j = 0; j < storage.CatalogOfProduct.Count - 1; j++)
                        {
                            if (dateOfCheck > shelfAll.products[i].EndDateProduct)
                            {
                                {
                                    if (shelfAll.products[i].ProductName == storage.CatalogOfProduct[j].ProductName)
                                        shelfAll.products[i].Count = 0;
                                    storage.CatalogOfProduct[j].Count = 0;
                                }
                            }
                        }
                        // Что-то не так
                        //Console.WriteLine("Removing goods with expired date:\n");
                        //Console.WriteLine($"Expired {shelfAll.products[i].ProductName} removed from store shelves.");
                        //Console.WriteLine($"Expired {shelfAll.products[i].ProductName} removed from storage shelves.");
                    }
                    // Удаление просроченых продуктов с полок.
                    DeleteExpiredProductAtStore();
                    // Удаление просроченых продуктов со склада.
                    DeleteExpiredProductAtStorage();
                    // Пиглашаем покупателей по очереди.
                    Buyer buyer = queueCostumers.Buyers.Dequeue();
                    // Создаем новый чек.
                    IdCheck++;
                    Check check = new Check(IdCheck, buyer.NameBuyer, buyer.ID, dateOfCheck);
                    double sumOfCheck = 0;
                    string message = "";
                    Console.WriteLine("\nWaiting for the customer!\n");
                    // Показываем данные покупателя
                    Console.WriteLine("Name of customer: " + buyer.NameBuyer + "\n" + "His money in vollet: " + buyer.Vollet);
                    Console.WriteLine("His list of wishes:\n");
                    foreach (var element in buyer.GoodsList)
                    {
                        Console.WriteLine(element.NameProduct + " - " + element.CountProduct + " pieces.");
                    }
                    Console.WriteLine();
                    // Сверяем Список товаров покyпателя с товаром на поке.
                    foreach (var item in buyer.GoodsList)
                    {
                        foreach (var element in shelfAll.products)
                        {
                            if (!(element.ProductName == item.NameProduct))
                            {
                                message = "There isn't " + item.NameProduct + " in the store";// перезаписывает пока                          
                            }
                            else if (element.ProductName == item.NameProduct && element.Count == 0)
                            {
                                Console.WriteLine($"{element.ProductName} is out of stock in the store");
                                PutProductOnSelf();
                            }
                            else if (element.ProductName == item.NameProduct && element.Count > 0)
                            {
                                double countProdOnShelf = element.Count;
                                double countProdInList = item.CountProduct;
                                if (countProdOnShelf > countProdInList || countProdOnShelf == countProdInList)
                                {
                                    double countProduct = countProdInList;
                                    int ID = element.ID;
                                    string nameProduct = element.ProductName;
                                    double price = element.Price;
                                    double sumArticul = countProduct * price;
                                    ListOfBuys listOfBuys = new ListOfBuys(nameProduct, countProduct, price, ID, sumArticul);
                                    check.ListOfBuys.Add(listOfBuys);
                                    sumOfCheck += sumArticul;
                                }
                                else
                                {
                                    double countProduct = countProdOnShelf;
                                    int ID = element.ID;
                                    string nameProduct = element.ProductName;
                                    double price = element.Price;
                                    double sumArticul = countProduct * price;
                                    Console.WriteLine($"There are only {countProduct} items of {item.NameProduct} in the store.");
                                    ListOfBuys listOfBuys = new ListOfBuys(nameProduct, countProduct, price, ID, sumArticul);
                                    check.ListOfBuys.Add(listOfBuys);
                                    sumOfCheck += sumArticul;
                                }
                            }
                        }
                    }
                    Console.WriteLine(message);// работает не правильно, перезаписывает
                    check.SumOfCheck = sumOfCheck;
                    statistic.OneCheck.Add(check);
                    statistic.AllChecksBuyer.Add(check);
                    ShowCheck();
                    string isPayed = "Yes";
                    check.CheckIsPayed = isPayed;
                    Console.WriteLine("Will you pay ?");
                    Console.WriteLine("Enter: Yes/No");
                    check.CheckIsPayed = isPayed = Console.ReadLine();
                    if (isPayed == "No")
                    {
                        Console.WriteLine("-------------------------");
                        Console.WriteLine("\nThe check is canceled\n");
                        Console.WriteLine("-------------------------");
                        Console.WriteLine("Enter any key to continue");
                        Console.ReadKey();
                    }
                    else
                    {
                        foreach (Check item in statistic.OneCheck)
                        {
                            if (buyer.Vollet < check.SumOfCheck)
                            {
                                isPayed = "No";
                                check.CheckIsPayed = isPayed;
                                Console.WriteLine("\nYou don't have money enough.\n");
                                Console.WriteLine("Enter any key to continue");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("\nThank you for your purchase.\n");
                                Console.WriteLine("Enter any key to continue");
                                Console.ReadKey();
                            }
                        }
                        DeleteCheck();
                        TakeProductFromSelf();
                    }
                    DeleteOneCheck();
                    Console.Clear();
                } while (queueCostumers.Buyers.Count != 0);
            }
        }
        public void ShowCheck()
        {
            foreach (Check item in statistic.OneCheck)
            {
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("------------- N Chek: " + item.IDCheck + " -------------" + " \n" + "Name of customer is: " + item.NameOfCustomer + "\nChek: ");
                foreach (var can in item.ListOfBuys)
                {
                    Console.WriteLine("Articul: " + can.NameProduct + ":  " + can.CountProduct + " x " + can.Price + " = " + can.SumOfProduct);
                }
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("AMOUNT" + "\t\t" + item.SumOfCheck);
                Console.WriteLine("-------------------------------------\n");
            }
        }
        public void DeleteCheck()
        {
            for (int i = statistic.OneCheck.Count - 1; i >= 0; i--)
            {
                if (statistic.OneCheck[i].CheckIsPayed == "No")
                    statistic.OneCheck.RemoveAt(i);
            }
        }
        public void DeleteOneCheck()
        {
            for (int i = statistic.OneCheck.Count - 1; i >= 0; i--)
            {
                statistic.OneCheck.RemoveAt(0);
            }
        }
        public void TakeProductFromSelf()
        {
            foreach (var element in shelfAll.products)
            {
                foreach (var item in statistic.OneCheck)
                {
                    foreach (var elem in item.ListOfBuys)
                    {
                        if (element.ProductName == elem.NameProduct)
                        {
                            element.Count = element.Count - elem.CountProduct;
                        }

                    }
                }
            }
        }
        /// <summary>
        /// Метод добавляет продукты на полку
        /// </summary>
        public void PutProductOnSelf()
        {
            for (int i = 0; i < shelfAll.products.Count - 1; i++)
            {
                for (int j = 0; j < storage.CatalogOfProduct.Count - 1; j++)
                {
                    if (shelfAll.products[i].ProductName == storage.CatalogOfProduct[j].ProductName)
                    {
                        if (shelfAll.products[i].Count == 0 && storage.CatalogOfProduct[j].Count > 0)
                        {
                            shelfAll.products[i].Count = 3;
                            storage.CatalogOfProduct[j].Count = storage.CatalogOfProduct[j].Count - shelfAll.products[i].Count;
                            Console.WriteLine($"{shelfAll.products[i].ProductName} was brought to the store from the storage.");
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Метод удаляет просроченную продукцию со склада.
        /// </summary>
        public void DeleteExpiredProductAtStorage()
        {
            for (int i = storage.CatalogOfProduct.Count - 1; i <= 0; i++)
            {
                if (storage.CatalogOfProduct[i].Count == 0)
                {
                    Console.WriteLine(storage.CatalogOfProduct[i]);
                    storage.CatalogOfProduct.RemoveAt(i);
                    Console.WriteLine($"An order has been made for products to suppliers.");
                }
            }
        }
        public void DeleteExpiredProductAtStore()
        {
            for (int j = storage.CatalogOfProduct.Count - 1; j >= 0; j--)
            {
                for (int i = shelfAll.products.Count - 1; i >= 0; i--)
                {
                    if (shelfAll.products[i].ProductName == storage.CatalogOfProduct[j].ProductName)
                    {
                        if (shelfAll.products[i].Count == 0 && storage.CatalogOfProduct[j].Count == 0)
                        {
                            shelfAll.products.RemoveAt(i);
                            Console.WriteLine($"An order has been made for products to suppliers.");
                        }
                    }
                }
            }
        }
    }
}

