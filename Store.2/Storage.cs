﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace store._2
{
    public class Storage
    {
        public static DateTime Time = DateTime.Now;
        public List<Product> CatalogOfProduct = new List<Product>();
        // Добавляем продукт в категорию молочных продуктов
        // ID, Наименование, вид, цена, количество, срок реализции, размер продукта, дата изготовления
        Product ProductFirst = new Product(1, "Milk", "MilkProducts", 20, 18, 2, 5, Time);
        Product ProductSecond = new Product(2, "Yogurt", "MilkProducts", 30, 9, 5, 3, Time);
        Product ProductThird = new Product(3, "Cheese", "MilkProducts", 40.0, 9, 3, 1, Time);
        //Добавляем продукты в категорию напитков.
        Product ProductFourth = new Product(4, "Lemonad", "DrinksProducts", 24.7, 10.0, 7, 15, Time);
        Product ProductFifth = new Product(5, "Water", "DrinksProducts", 20.0, 10.0, 10, 20, Time);
        Product ProductSixth = new Product(6, "Colla", "DrinksProducts", 30.0, 10.0, 10, 15, Time);
        //Добавляем продукты в категорию овощей.
        Product ProductSeventh = new Product(7, "Cucumbers", "VegetablesProducts", 61.2, 3, 20, 10, Time);
        Product ProductEights = new Product(8, "Potatoes", "VegetablesProducts", 46, 10, 5, 20, Time);
        Product ProductNineth = new Product(9, "Tomatto", "VegetablesProducts", 30.0, 10.0, 10, 5, Time);
        // Добавляем продукты в категорию фруктов.
        Product ProductTenth = new Product(10, "Cheryy", "FruitsProducts", 24.7, 10.0, 7, 11, Time);
        Product ProductEleventh = new Product(11, "Strawberry", "FruitsProducts", 20.0, 10.0, 10, 10, Time);
        Product ProductTwelfth = new Product(12, "Oranges", "FruitsProducts", 30.0, 10.0, 10, 10, Time);
        // Добавляем продукты в категорию хлеба.
        Product ProductThirteenth = new Product(13, "Bread", "BreadProducts", 24.7, 10.0, 2, 15, Time);
        Product ProductFourteenth = new Product(14, "Loaf", "BreadProducts", 20.0, 10.0, 2, 15, Time);
        Product ProductFifteenth = new Product(15, "Buns", "BreadProducts", 30.0, 10.0, 3, 10, Time);
        // Добавляем продукты в категорию сладостей.
        Product ProductSixteenth = new Product(16, "Candy", "SweetsProducts", 24.7, 10.0, 7, 5, Time);
        Product ProductSeventeenth = new Product(17, "Сhocolate", "SweetsProducts", 20.0, 10.0, 20, 5, Time);
        Product ProductEightinteenth = new Product(18, "Сookies", "SweetsProducts", 30.0, 10.0, 10, 5, Time);
        public Storage()
        {
            // Milks.
            CatalogOfProduct.Add(ProductFirst);
            CatalogOfProduct.Add(ProductSecond);
            CatalogOfProduct.Add(ProductThird);
            // Drinks.
            CatalogOfProduct.Add(ProductFourth);
            CatalogOfProduct.Add(ProductFifth);
            CatalogOfProduct.Add(ProductSixth);
            // Vegetables.
            CatalogOfProduct.Add(ProductSeventh);
            CatalogOfProduct.Add(ProductEights);
            CatalogOfProduct.Add(ProductNineth);
            // Fruits.
            CatalogOfProduct.Add(ProductTenth);
            CatalogOfProduct.Add(ProductEleventh);
            CatalogOfProduct.Add(ProductTwelfth);
            // Bread.
            CatalogOfProduct.Add(ProductThirteenth);
            CatalogOfProduct.Add(ProductFourteenth);
            CatalogOfProduct.Add(ProductFifteenth);
            // Sweets.
            CatalogOfProduct.Add(ProductSixteenth);
            CatalogOfProduct.Add(ProductSeventeenth);
            CatalogOfProduct.Add(ProductEightinteenth);
        }
    }
}
