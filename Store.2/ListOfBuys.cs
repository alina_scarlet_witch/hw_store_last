﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace store._2
{
    public class ListOfBuys
    {
        public int ID;
        public string NameProduct;
        public double CountProduct;    
        public double Price;
        public double SumOfProduct;
        public ListOfBuys(string nameProduct, double countProduct, double price, int iD, double sumOfProduct)
        {       
            NameProduct = nameProduct;
            CountProduct = countProduct;
            Price = price;
            ID = iD;
            SumOfProduct=sumOfProduct;
        }
    }
}
